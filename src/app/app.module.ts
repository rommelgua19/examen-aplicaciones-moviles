import { environment } from './../environments/environment';
import { Item4Service } from './api/item4.service';
import { Item3Service } from './api/item3.service';
import { Item2Service } from './api/item2.service';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AuthService} from 'src/app/api/auth.service';
import {ItemService} from 'src/app/api/item.service';
import {NativeStorage} from '@ionic-native/native-storage/ngx';
import { AngularFireModule } from '@angular/fire';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
  AngularFireModule.initializeApp(environment.firebase),
],
  providers: [
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    AuthService,
    ItemService,
    NativeStorage,
    Item2Service,
    Item3Service,
    Item4Service
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
