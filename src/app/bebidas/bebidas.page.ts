import { Router } from '@angular/router';
import { Item3Service } from 'src/app/api/item3.service';
import { ItemModel } from 'src/app/model/item.model';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-bebidas',
  templateUrl: './bebidas.page.html',
  styleUrls: ['./bebidas.page.scss'],
})
export class BebidasPage implements OnInit {
  public bebidas: ItemModel[];

  constructor(private item3Service: Item3Service, private rout: Router) {
  }

  ngOnInit() {
      this.synch();
  }

  synch(): void {
    this.item3Service.getAll().subscribe(data=>{
      this.bebidas=data
      console.log(data)
  })
  }

  navigateToDetail(id: string): void {
      this.rout.navigate(['/item3/' + id]);
  }

}
