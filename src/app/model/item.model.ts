export interface ItemModel {
    id?: string;
    name?: string;
    description?: string;
    imageURL?: string;
    price?: number;
}
