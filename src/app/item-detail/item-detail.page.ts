import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NavController} from '@ionic/angular';
import {ItemService} from 'src/app/api/item.service';
import {ItemModel} from 'src/app/model/item.model';
import {ShoppingItemModel} from 'src/app/model/shopping-item.model';
import {ShoppingHelpModel} from '../model/shopping-help.model';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.page.html',
  styleUrls: ['./item-detail.page.scss'],
})
export class ItemDetailPage implements OnInit {
  public id: string;
  public item: ItemModel;
  public shoppingItem: ShoppingItemModel;
  public shoppingHelp: ShoppingHelpModel = {total: 0, items: 0};

  constructor(private route: ActivatedRoute, private navCtrl: NavController,
              private itemService: ItemService,  ) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.item = {};
    this.shoppingItem = {amount: 0};
  }

  ngOnInit() {
    this.getItemById();
  }

  return(): void {
    this.navCtrl.back();
  }

  getItemById(): void {
    if (this.id != null) {
      this.itemService.getById(this.id).subscribe(data=>{
        this.item=data
      })
    }
  }

 

  


}
