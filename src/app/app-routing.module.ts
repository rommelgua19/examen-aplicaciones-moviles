
import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
},
{
  path: '',
  redirectTo: 'tabs',
  pathMatch: 'full'
},
    {
        path: 'item-detail/:id',
        loadChildren: () => import('./item-detail/item-detail.module').then(m => m.ItemDetailPageModule)
    },
    {
      path: 'item2/:id',
      loadChildren: () => import('./item2/item2.module').then(m => m.Item2PageModule)
  },
  {
    path: 'item3/:id',
    loadChildren: () => import('./item3/item3.module').then(m => m.Item3PageModule)
},
{
  path: 'item4/:id',
  loadChildren: () => import('./item4/item4.module').then(m => m.Item4PageModule)
},

  {
    path: 'bebidas',
    loadChildren: () => import('./bebidas/bebidas.module').then( m => m.BebidasPageModule)
  },
  {
    path: 'postres',
    loadChildren: () => import('./postres/postres.module').then( m => m.PostresPageModule)
  },
  {
    path: 'item3',
    loadChildren: () => import('./item3/item3.module').then( m => m.Item3PageModule)
  },
  {
    path: 'item4',
    loadChildren: () => import('./item4/item4.module').then( m => m.Item4PageModule)
  },



];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
