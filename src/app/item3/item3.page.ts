import { ShoppingItemModel } from './../model/shopping-item.model';
import { ShoppingHelpModel } from './../model/shopping-help.model';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ItemModel } from './../model/item.model';
import { Item3Service } from 'src/app/api/item3.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-item3',
  templateUrl: './item3.page.html',
  styleUrls: ['./item3.page.scss'],
})
export class Item3Page implements OnInit {
  public id: string;
  public item: ItemModel;
  public shoppingItem: ShoppingItemModel;
  public shoppingHelp: ShoppingHelpModel = {total: 0, items: 0};
  
  constructor(private route: ActivatedRoute, private navCtrl: NavController,
    private item3Service: Item3Service,  ) {
this.id = this.route.snapshot.paramMap.get('id');
this.item = {};
this.shoppingItem = {amount: 0};
}

ngOnInit() {
this.getItemById();
}

return(): void {
this.navCtrl.back();
}

getItemById(): void {
  if (this.id != null) {
    this.item3Service.getById(this.id).subscribe(data=>{
      this.item=data
    })
  }
}

}
