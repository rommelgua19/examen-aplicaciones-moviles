import { ShoppingHelpModel } from './../model/shopping-help.model';
import { ShoppingItemModel } from './../model/shopping-item.model';
import { ItemModel } from './../model/item.model';
import { Item2Service } from './../api/item2.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-item2',
  templateUrl: './item2.page.html',
  styleUrls: ['./item2.page.scss'],
})
export class Item2Page implements OnInit {
  public id: string;
  public item: ItemModel;
  public shoppingItem: ShoppingItemModel;
  public shoppingHelp: ShoppingHelpModel = {total: 0, items: 0};
  
  constructor(private route: ActivatedRoute, private navCtrl: NavController,
    private item2Service: Item2Service,  ) {
this.id = this.route.snapshot.paramMap.get('id');
this.item = {};
this.shoppingItem = {amount: 0};
}

ngOnInit() {
this.getItemById();
}

return(): void {
this.navCtrl.back();
}

getItemById(): void {
  if (this.id != null) {
    this.item2Service.getById(this.id).subscribe(data=>{
      this.item=data
    })
  }
}

}
