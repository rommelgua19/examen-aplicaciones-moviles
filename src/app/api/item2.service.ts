import { ItemModel } from 'src/app/model/item.model';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import {map} from "rxjs/operators"
@Injectable({
  providedIn: 'root'
})
export class Item2Service {
  

    private colletion:AngularFirestoreCollection<ItemModel>
    private listMenu:Observable<ItemModel[]>

    


    constructor(private firestore:AngularFirestore) {
        this.colletion=this.firestore.collection<ItemModel>("entradas")
    }

    public getAll(): Observable<ItemModel[]> {
        this.listMenu=this.colletion.snapshotChanges().pipe(
            map(data=>data.map(platos=>{
                const document =platos.payload.doc.data()
                const id = platos.payload.doc.id
                let food = {id,...document}
                return food
            }))
        )
        return this.listMenu
    }
    public getById(id:string){
        return this.colletion.doc(id).snapshotChanges().pipe(
            map(data=>{
                const document =data.payload.data()
                const id = data.payload.id
                let food = {id,...document}
                return food
            })
        )
    }

}
