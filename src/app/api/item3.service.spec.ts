import { TestBed } from '@angular/core/testing';

import { Item3Service } from './item3.service';

describe('Item3Service', () => {
  let service: Item3Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Item3Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
