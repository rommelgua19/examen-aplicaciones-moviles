import { TestBed } from '@angular/core/testing';

import { Item2Service } from './item2.service';

describe('Item2Service', () => {
  let service: Item2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Item2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
