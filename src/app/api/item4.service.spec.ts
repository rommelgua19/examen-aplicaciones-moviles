import { TestBed } from '@angular/core/testing';

import { Item4Service } from './item4.service';

describe('Item4Service', () => {
  let service: Item4Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Item4Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
