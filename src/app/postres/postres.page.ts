import { Item4Service } from './../api/item4.service';
import { Router } from '@angular/router';
import { ItemModel } from 'src/app/model/item.model';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-postres',
  templateUrl: './postres.page.html',
  styleUrls: ['./postres.page.scss'],
})
export class PostresPage implements OnInit {
  public postres: ItemModel[];

  constructor(private item4Service:Item4Service, private rout: Router) {
  }

  ngOnInit() {
      this.synch();
  }

  synch(): void {
    this.item4Service.getAll().subscribe(data=>{
      this.postres=data
      console.log(data)
  });
  }

  navigateToDetail(id: string): void {
      this.rout.navigate(['/item4/' + id]);
  }

}
