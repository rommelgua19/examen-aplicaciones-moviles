import { Item2Service } from './../api/item2.service';
import { ItemModel } from 'src/app/model/item.model';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-platos-fuertes',
  templateUrl: './platos-fuertes.page.html',
  styleUrls: ['./platos-fuertes.page.scss'],
})
export class PlatosFuertesPage implements OnInit {
  public platosFuertes: ItemModel[];

    constructor(private item2Service: Item2Service, private rout: Router) {
    }

    ngOnInit() {
        this.synch();
    }

    synch(): void {
      this.item2Service.getAll().subscribe(data=>{
        this.platosFuertes=data
        console.log(data)
    });
    }

    navigateToDetail(id: string): void {
        this.rout.navigate(['/item2/' + id]);
    }

}
