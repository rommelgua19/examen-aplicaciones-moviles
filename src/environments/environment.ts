// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBZAVovblfvj9NVBtQ2-BMdwHV0rNWn7SI",
    authDomain: "restaurante-e1667.firebaseapp.com",
    projectId: "restaurante-e1667",
    storageBucket: "restaurante-e1667.appspot.com",
    messagingSenderId: "601887979314",
    appId: "1:601887979314:web:b8ac224fc990c1f3bb492b",
    measurementId: "G-2PXPQDJW6H"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
